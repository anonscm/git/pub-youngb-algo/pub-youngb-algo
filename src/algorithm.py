# -*- coding: utf-8 -*-
"""
This module regroups the classes and functions used to run the algorithm of
batch optimization.

It is structured as follows:

The class 'Batch' regroups informations about the composition of the batches.

The functions used to perform the distance optimization:

    -run_batch_optimization: the main function calling the other functions and
    formatting the output.

    -assign: perform a set of multiple assignments, until there is no batch
    left to create or no calf left to assign.

    -assignment_list: perform a series of assignments over a list of breeders
    and calves.

    -generate_intervals: creates an array with the starting and ending date of
    each interval.

    -match_to_sum: select a set of elements in an array matching a given value.

"""

import itertools
import numpy as np # version 1.15.1
import pandas as pd # version 0.23.4


class Batch:
    """
    The batch of bovines with its composition in calves.

    Parameters
    ----------
    row: a row from the dataset of bovines

    """

    def __init__(self, row):
        self.name = row.name
        self.bovines = []
        self.nb_bovines = row.nb_bovines
        self.date = row.batch_date
        self.breed = row.main_breed
        self.prop = row.purity

    @property
    def current_size(self):
        """
        Returns the number of bovines assigned to the batch.
        """
        return len(self.bovines)

    @property
    def is_breed(self):
        """
        A boolean indicating whether there is enough bovines of the main breed.
        """
        return self.current_size >= self.min_size

    @property
    def min_size(self):
        """
        Returns the minimal number of calves of the main breed required to
        match the batch characteristics.
        """
        return int(np.ceil(self.prop*self.nb_bovines))

    @property
    def remaining(self):
        """
        Returns the number of calves to assign to reach the expected size of
        the batch.
        """
        return self.nb_bovines - len(self.bovines)


    def add_bovines(self, l_new):
        """
        Add bovines to the list of bovines included in the batch.

        Parameters
        ----------
        l_new: the list of bovines to add

        Returns
        -------
        The function returns nothing.

        """
        self.bovines.extend(l_new)


def run_batch_optimization(df_data, d_batches, is_carry_over, interval=None):
    """
    Runs the algorithm of batch optimization.

    Parameters
    ----------
    df_data: a dataframe with the data
    d_batches: a dictionnary with the batches to create
    is_carry_over: should the calves and batches be carried over to the next
    interval?
    interval: the size of the time-window (in days)

    Returns
    -------
    The function returns nothing.

    Relationship to other functions
    -------------------------------
    Calls the following function(s):
        -assign

    """
    # list the interval starting dates
    if interval is None and is_carry_over:
        # no carry-on is possible if there is no interval
        is_carry_over = False
        raise Warning("No carry-on possible if there is no interval.")
    time_interval = pd.Timedelta(interval, 'D')

    date_range = generate_intervals(df_data.arrival_date, df_data.exit_date,
                                    time_interval)

    for start, end in date_range:
        #start, end = date_range[0]
        # identify the calves that must be assigned in the interval
        to_assign = df_data.loc[(df_data.arrival_date >= start) &
                                (df_data.exit_date < end)]
        # identify the list of batches to create in the interval
        d_this_date = {i : batch for i, batch in d_batches.items() if
                       (batch.date >= start) & (batch.date < end)}.copy()

        # list the breeds
        a_breeds = np.unique([batch.breed for batch in d_this_date.values() if
                              str(batch.breed) != 'nan'])

        # assign the batches of the right breed
        for breed in a_breeds:
            # array of batches with the breed
            d_breed = {i: batch for i, batch in d_this_date.items() if
                       batch.breed == breed}
            # compute the number of calves of the main breed to assign
            a_sizes = [batch.min_size for batch in  d_breed.values()]
            a_batches = np.vstack(([*d_breed.keys()], a_sizes))

            # assign the animals
            a_assigned = assign(a_batches, to_assign, breed)

            # remove the list of assigned calves from the calves and the batches
            to_assign = to_assign.drop(a_assigned[0])
            for batch in set(a_assigned[2]):
                is_batch = a_assigned[2] == batch
                d_this_date[batch].add_bovines(a_assigned[0, is_batch])

        # list of batches which recieved their number of animals of the breed
        d_to_complete = {key : value for key, value in d_this_date.items() if
                         value.is_breed}

        # define the remaining batches
        a_remaining = np.array([[batch.name, batch.remaining] for
                                batch in d_to_complete.values()]).T
        # complete the batches
        a_assigned = assign(a_remaining, to_assign)
        to_assign = to_assign.drop(a_assigned[0])
        for batch in set(a_assigned[2]):
            is_batch = a_assigned[2] == batch
            d_to_complete[batch].add_bovines(a_assigned[0, is_batch])

        # check the non complete batches
        l_missing_batch = [key for key, value in d_this_date.items() if
                           value.remaining > 0]

        for key in l_missing_batch:
            # report the calves and batches to the next interval if needed
            if is_carry_over:
                # carry-on the calves assigned to the incomplete batch
                l_calves = d_batches[key].bovines
                df_data.loc[l_calves, "exit_date"] += time_interval
                df_data.loc[l_calves, "arrival_date"] += time_interval
                # carry-on the incomplete batch
                d_batches[key].date += time_interval
            # remove the calves from the incomplete batch
            d_batches[key].bovines = []

        if is_carry_over:
            # report the unassigned calves to the next interval if needed
            df_data.loc[to_assign.index, "exit_date"] += time_interval
            df_data.loc[to_assign.index, "arrival_date"] += time_interval


def generate_intervals(arrival_dates, exit_dates, time_interval):
    """
    Creates an array with the starting and ending date of each interval.

    Parameters
    ----------
    arrival_dates: a pandas series of the arrival dates of the calves
    exit_dates: a pandas series of the exit dates of the calves
    time_interval: a pandas timedelta of the size of the interval

    Returns
    -------
    Returns an array of the interval limits

    Relationship to other functions
    -------------------------------
    Called by the following function(s):
        -run_batch_optimization

    """
    # check if there is a time interval
    min_limit = min(arrival_dates)
    if pd.isnull(time_interval):
        max_limit = max(exit_dates) + pd.Timedelta(1, 'D')
        date_range = np.array([[min_limit], [max_limit]])
    else:
        min_limit = min(arrival_dates)
        max_limit = max(exit_dates) + pd.Timedelta(1, 'D')
        start_date = pd.date_range(min_limit, max_limit, freq=time_interval)
        end_date = start_date + time_interval
        date_range = np.vstack((start_date, end_date))

    return date_range.T


def assign(a_batches, to_assign, breed=None):
    """
    Perform a set of multiple assignments, until there is no batch left to
    create or no calf left to assign.

    Parameters
    ----------
    a_batches: an array of the batches to create with their size
    to_assign: a table of the calves considered for assignation
    breed: the breed considered, if any

    Returns
    -------
    Returns an array of all the assignments performed

    Relationship to other functions
    -------------------------------
    Called by the following function(s):
        -run_batch_optimization
    Calls the following function(s):
        -assignment_list

    """
    # array of breeders with their size
    if breed is None:
        origins = to_assign.origin.copy(deep=True)
    else:
        origins = to_assign.loc[to_assign.breed == breed,
                                'origin'].copy(deep=True)
    a_breeders = np.array(np.unique(origins, return_counts=True))
    a_output = np.empty((3, 0), dtype='int')

    while True:
        # stop if there is no batch at all
        if not a_batches.size:
            break
        # ignore batches larger than the number of calves available
        a_batches[1, a_batches[1] > origins.size] = 0
        # stop if there is no batch or no calves left to assign
        if sum(a_batches[1]) == 0 or origins.size == 0:
            break
        a_assigned = assignment_list(a_breeders, a_batches, origins)
        # remove from breeders
        for breeder, n in np.array(np.unique(a_assigned[1],
                                             return_counts=True)).T:
            a_breeders[1, np.argmax(a_breeders[0] == breeder)] -= n
            origins = origins.drop(origins.index[origins == breeder][:n])
        # remove from batches
        for batch, n in np.array(np.unique(a_assigned[2],
                                           return_counts=True)).T:
            a_batches[1, np.argmax(a_batches[0] == batch)] -= n
        a_output = np.hstack([a_output, a_assigned])

    return a_output


def assignment_list(a_breeders, a_batches, origins):
    """
    Perform a single assignment based on a list of breeders and calves.

    Parameters
    ----------
    a_breeders: an array with the number and size of the breeders
    a_batches: an array with the number and size of the batches
    origins: the calves with their origin

    Returns
    -------
    Returns an array with the list of calves assigned, the breeder they come
    from and the batch they were assigned to.

    Relationship to other functions
    -------------------------------
    Called by the following function(s):
        -assign
    Calls the following function(s):
        -match_to_sum

    """
    if max(a_breeders[1]) < max(a_batches[1]):
        # select the batch to complete
        max_idx = np.argmax(a_batches[1])
        max_batch = a_batches[0, max_idx]
        max_value = a_batches[1, max_idx]
        a_breeders = a_breeders[:, np.argsort(a_breeders[1])[::-1]]
        # select the breeders of the bovines to assign
        indices = match_to_sum(a_breeders[1], max_value, +1)
        a_selected = a_breeders[:, indices]
        a_selected[1, [-1]] -= (sum(a_selected[1]) - max_value)
        # select the bovines to assign
        a_bovines = np.hstack([origins.index[origins == i][:nb] for
                               i, nb in a_selected.T])
        a_assigned = np.vstack((a_bovines,
                                np.repeat(a_selected[0], a_selected[1]),
                                np.repeat(max_batch, max_value)))
    else:
        #raise ValueError('Ok')
        # select the breeder to assign
        max_idx = np.argmax(a_breeders[1])
        max_breeder = a_breeders[0, max_idx]
        max_value = a_breeders[1, max_idx]
        a_batches = a_batches[:, np.argsort(a_batches[1])[::-1]]
        # select the batches to complete
        indices = match_to_sum(a_batches[1], max_value, -1)
        # select the bovines to assign
        a_selected = a_batches[:, indices]
        a_bovines = origins.index[origins == max_breeder][:sum(a_selected[1])]
        a_assigned = np.vstack((a_bovines,
                                np.repeat(max_breeder, sum(a_selected[1])),
                                np.repeat(a_selected[0], a_selected[1])))
    return a_assigned


def match_to_sum(a_elements, value, move):
    """
    Select a set of elements in an array matching a given value.

    Parameters
    ----------
    a_elements: an array of elements to choose from to match the value
    value: the value to match
    move: the direction in which the value must change if there is not match.
    -1 will decrease the value by 1 at each new iteration, +1 will increase
    the value by 1.

    Returns
    -------
    The function returns the indexes of the elements chosen.

    Relationship to other functions
    -------------------------------
    Called by the following function(s):
        -assignment_list

    """

    #Remove the elements whose value is 0
    a_nonzero = a_elements[a_elements > 0]
    while True:
        #Select the minimal number of elements necessary to reach the value
        for kmin in range(len(a_nonzero)):
            if sum(a_nonzero[:kmin]) >= value:
                break
        for k in range(kmin-1, len(a_nonzero)):
            #Identify the elements necessary to reach the value
            a_indices = np.indices(a_nonzero.shape)[0]
            if sum(a_nonzero[a_indices[::-1]][:k+1]) > value:
                i = (0,)
                break
            a_essential = []
            for i in a_indices:
                if sum(a_nonzero[a_indices != i][:k+1]) >= value:
                    break
                a_essential.append(i)
            #Iterate over every combination of k elements
            for i in itertools.combinations(a_indices, k+1):
                if sum([x not in i for x in a_essential]) > 0:
                    break
                if sum(a_nonzero[list(i)]) == value:
                    break
            if sum(a_nonzero[list(i)]) == value:
                break

        #Return the output if it is found, change the value otherwise
        if sum(a_nonzero[list(i)]) != value:
            value += move
        else:
            return np.flatnonzero(a_elements > 0)[list(i)]

############################### END OF THE CODE ################################
