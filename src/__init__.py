# -*- coding: utf-8 -*-
"""
Initializes the main codeo of the algorithm of batch optimization.

Author: Thibaut Morel-Journel

Written for Python 3.7

"""
from src.algorithm import Batch
from src.algorithm import run_batch_optimization

############################### END OF THE CODE ################################
