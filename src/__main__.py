# -*- coding: utf-8 -*-
"""
Runs the the algorithm of batch optimization.

Author: Thibaut Morel-Journel

Written for Python 3.7

"""

# import the modules
import argparse
import os
import numpy as np # version 1.15.1
import pandas as pd # version 0.23.4
import sys

# add the path to the current directory
sys.path.append(os.path.abspath(os.path.curdir))
import src # the code for the algorithm


def check_results(df_expected, df_outcome):
    """
    Check if the results from the test correspond to the expected outcomes.

    Parameters
    ----------
    df_expected: a table with the expected outcomes
    df_outcome: a table with the actual outcomes

    Returns
    -------
    Indicates if there are mismatches between the two tables, if not, prints
    'all outcomes correspond to expected'

    """
    print('Checking the test results:')
    no_error = True
    for col in df_expected.columns:
        l_errors = [i for i, row in df_outcome.iterrows() if
                    row.loc[col] != df_expected.loc[i, col]]
        for i in l_errors:
            print("    - error in row '" + str(i) + "'and column '" +  col
                  + "'")
            no_error = False
    if no_error:
        print('    - all outcomes correspond to expected')


# argument parser for the script
parser = argparse.ArgumentParser(description="""Runs the algorithm of batch
                                 optimization""")
parser.add_argument('folder', type=str,
                    help="""the name of the folder containing the data""")
parser.add_argument('-i', '--interval', type=int,
                    help="""the duration of the time period to consider at
                    once""")
parser.add_argument('-c', '--carryover', action='store_true',
                    help="""carry over the excess calves or batches to the next
                    interval""")

FOLDER = vars(parser.parse_args())['folder']
is_carry_over = vars(parser.parse_args())['carryover']
is_test = FOLDER == 'test'

if is_test:
    INTERVAL = 3 # set value of the interval for the test
    data_path = './test/data/'
    print(data_path)
else:
    INTERVAL = vars(parser.parse_args())['interval']
    data_path = './data/' + FOLDER # path to the data files


print("\nAlgorithm of batch optimization for the dataset '" + FOLDER + "':")

# load the dataset
print("  Loading the data")
try:
    df_data = pd.read_csv(data_path + '/dataset.csv', index_col=0,
                          parse_dates=['arrival_date', 'exit_date'])
except FileNotFoundError:
    raise ValueError('The dataset of bovines is missing')

try:
    df_batches = pd.read_csv(data_path + '/batch_list.csv', index_col=0,
                             parse_dates=['batch_date'])
except FileNotFoundError:
    raise ValueError('The dataset of batches is missing')

# check and remove rows with missing values
l_col_data = ['exit_date']
d_missing_values = {name: col.index[col.isna()] for
                    name, col in df_data.loc[:, l_col_data].iteritems()}
for col, indices in d_missing_values.items():
    for index in indices:
        df_data = df_data.drop(index)
        print("    - row '" + str(index) + "' removed because of missing " +
              "value for '" + col + "'")
l_col_batches = ['nb_bovines', 'batch_date', 'destination', 'purity']
d_missing_values = {name: col.index[col.isna()] for
                    name, col in df_batches.loc[:, l_col_batches].iteritems()}
for col, indices in d_missing_values.items():
    for index in indices:
        df_data = df_batches.drop(index)
        print("    - row '" + str(index) + "' removed because of missing " +
              "value for '" + col + "'")

# create a dictionnary of the batches to create
d_batches = {i: src.Batch(row) for i, row in df_batches.iterrows()}

print("  Assigning individuals to the batches")
# assign the individuals to the batches
src.run_batch_optimization(df_data, d_batches, is_carry_over, INTERVAL)

print("  Computing the number of origins per batch")
# format the output data
df_output = df_data.loc[:, ['origin']]
for name, batch in d_batches.items():
    nb_origins = len(set(df_output.loc[batch.bovines, 'origin']))
    df_output.at[batch.bovines, 'batch'] = name
    df_output.at[batch.bovines, 'batch_size'] = batch.nb_bovines
    df_output.at[batch.bovines, 'nb_origins'] = nb_origins
# change the type of the columns if there is no na
if not any(df_output.batch.isna()):
    df_output.batch = df_output.batch.astype(type([*d_batches.keys()][0]))
if not any(df_output.batch_size.isna()):
    df_output.batch_size = df_output.batch_size.astype('int')
if not any(df_output.nb_origins.isna()):
    df_output.nb_origins = df_output.nb_origins.astype('int')

# check if the data includes pre-existing batches
try:
    print("  Computing the number of origins per batch from the dataset")
    df_output.at[df_data.index, 'batch_old'] = df_data.batch
    for batch in np.unique(df_data.batch):
        selected = df_output.batch_old == batch
        nb_origins = len(np.unique(df_data.loc[selected, "origin"]))
        df_output.at[selected, 'batch_size_old'] = sum(selected)
        df_output.at[selected, 'nb_origins_old'] = nb_origins
    df_output.batch_size_old = df_output.batch_size_old.astype('int')
    df_output.nb_origins_old = df_output.nb_origins_old.astype('int')
except:
    None

# save the output
if INTERVAL is None:
    INTERVAL = 'no_interval'
print("  Saving the output")
os.makedirs(os.getcwd() + '/outcomes/' + FOLDER, exist_ok=True)
df_output.to_csv('./outcomes/' + FOLDER + '/output_' + str(INTERVAL) + '.csv')


# run a check of the results if the folder is 'test'
if FOLDER == 'test':
    df_expected = pd.read_csv('./test/outcomes/expected_outcome.csv',
                              index_col=0)
    check_results(df_expected, df_output)

############################### END OF THE CODE ################################
