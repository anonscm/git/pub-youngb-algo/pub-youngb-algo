# -*- coding: utf-8 -*-
"""
Randomly generates the distribution of pathogens across cow-calf producers with
or without correlation.

Author: Thibaut Morel-Journel

Written for Python 3.7

"""

def generate_status(origins, df_pathogens, NB_REP, is_corr):
    """
    Generates the statuses of the cow-calf producers.

    Parameters
    ----------
    origin: a pandas series with the origins of the calves
    df_pathogens: a data frame with the pahtogen characteristics
    NB_REP: the number of repetitions generated
    is_corr: a boolean indicating whether the pathogen presence should be
    positively correalated

    Returns
    -------
    An array with the status of each cow-calf producer for each run

    Relationship to other functions
    -------------------------------
    Calls the following function(s):
        - generate_proba

    """
    # create the output
    a_output = np.zeros((len(df_pathogens), NB_REP, len(np.unique(origins))),
                        dtype="int")

    # draw the order in which the pathogens will be drawn
    a_order = np.array([np.random.choice(range(len(df_pathogens)),
                                         len(df_pathogens), False) for
                        rep in range(NB_REP)])

    # probabilities of pathogens for the first pathogen added
    a_proba = [df_pathogens.iloc[p, 0] for p in a_order[:, 0]]

    # draw the status of the first pathogen added
    a_status = np.array([np.random.binomial(1, p, len(np.unique(origins))) for
                         p in a_proba])
    # record the output
    a_output[(a_order[:, 0], np.arange(NB_REP))] = a_status

    for order in a_order.T[1:]:
        # probabilities of pathogens for the nth pathogen added
        a_proba = np.array([df_pathogens.iloc[p, 0] for p in order])
        a_proba_corr = generate_proba(a_output, a_proba, is_corr)
        # draw the status
        a_status = np.random.binomial(1, a_proba_corr)
        a_output[(order, np.arange(NB_REP))] = a_status

    return a_output


def generate_proba(a_presence, a_proba, is_corr):
    """
    Generate the probabilities for each pathogens, with or without correlation.

    Parameters
    ----------
    a_presence: an array indicating the presence (1) or absence (0) of each
    pathogen in each cow-calf producer for each repetition
    a_proba: an array of the probabilities of the pathogen to assign
    is_corr: a boolean indicating whether the pathogen presence should be
    positively correalated

    Returns
    -------
    An array with the probabilities of having the pathogen for each cow-calf
    producer and each run

    Relationship to other functions
    -------------------------------
    Called by the following function(s):
        -generate_status

    """
    if is_corr:
        # number of pathogens already present in the farm
        a_score = np.sum(a_presence, axis=0)
        # centered score
        a_score_ctr = (a_score.T - np.mean(a_score, axis=1))
        # score corrected for the intensity of the correlation
        a_score_corr = a_score_ctr/np.max(abs(a_score_ctr), axis=0)
        # factors by which the probabilities should be multiplied
        a_factor = np.min(np.vstack(((-a_proba)/np.min(a_score_corr, 0),
                                     (1 - a_proba)/np.max(a_score_corr, 0))),
                          axis=0)
        a_proba_corr = (a_factor*a_score_corr + a_proba).T
        # check which proba return 'nan'
        a_nan_proba = np.any(np.isnan(a_proba_corr), axis=1)
        for i in np.flatnonzero(a_nan_proba):
            a_proba_corr[i] = a_proba[i]
        # remove float error
        a_proba_corr = np.round(a_proba_corr, 12)
    else:
        a_proba_corr = np.broadcast_to(a_proba, a_presence.shape[1:][::-1]).T

    return a_proba_corr


if __name__ == '__main__':

    # import modules
    import argparse
    import pandas as pd # version 0.23.4
    import numpy as np # version 1.15.1

    # argument parser for the script
    parser = argparse.ArgumentParser(description="""Script to generate
                                     distributions of pathogens across cow-calf
                                     producers""")
    parser.add_argument('folder', type=str,
                        help="""the name of the folder containing the data""")
    parser.add_argument('rep', type=int,
                        help="""the number of repetitions""")
    parser.add_argument('-c', '--corr', action='store_true',
                        help="""should the pathogen presence be correlated""")

   # get the parameters
    FOLDER = vars(parser.parse_args())['folder']
    NB_RUN = vars(parser.parse_args())['rep']
    is_corr = vars(parser.parse_args())['corr']

    print("Generate the status of the farms for the folder '" +  FOLDER + "':")

    # load the pathogen characteristics
    try:
        df_pathogens = pd.read_csv('./data/' + FOLDER + '/pathogen_char.csv',
                                   index_col=0)
    except FileNotFoundError:
        raise ValueError('The pathogen values are missing')

    # load the pathogen characteristics
    try:
        df_data = pd.read_csv('./data/' + FOLDER + '/dataset.csv', index_col=0)
    except FileNotFoundError:
        raise ValueError('The pathogen values are missing')

    # generate breeder status
    a_status = generate_status(df_data.origin, df_pathogens, NB_RUN, is_corr)

    # save status
    np.save('./data/' + FOLDER + '/pathogen_status.npy', a_status)

############################### END OF THE SCRIPT ##############################
