# -*- coding: utf-8 -*-
"""
Randomly generates the distribution of pathogens across cow-calf producers and
computes the risk indices of each batch.

Author: Thibaut Morel-Journel

Written for Python 3.7

"""

def compute_risk(df_pathogens, batch_status):
    """
    Compute the partial risks of the different ways for the different pathogens.

    Parameters
    ----------
    df_pathogens: a data frame with the characteristics of the pathogens
    batch_status: an array with the presence of pathogens in the origins of each
    calf

    Returns
    -------
    An array with the partial indices (for each way and each pathogen, sum over
    each way, sum over each pathogen and total sum)

    Relationship with other functions
    ---------------------------------
    Called by the following function(s):
        -format_risks

    """
    # get the characteristics of the pathogens
    exposure = df_pathogens.exposure.values
    infection = df_pathogens.infection.values
    reinfection = df_pathogens.reinfection.values
    reactivation = df_pathogens.reactivation.values

    if len(batch_status) > 1:
        a_exposed = np.zeros(batch_status.shape, dtype="float")
        for i, status in enumerate(batch_status):
            # proportion of exposed individuals in the
            a_prop = np.mean(np.delete(batch_status, i, axis=0), axis=0)
            a_exposed[i] = 1 - np.exp(- a_prop*exposure)
    else:
        a_exposed = np.zeros_like(batch_status, dtype="float")

    # infection risk
    a_infection = (1 - batch_status)*infection*a_exposed

    # reinfection risk
    a_reinfection = batch_status*reinfection*a_exposed

    # reactivation risk
    a_reactivation = reactivation*batch_status

    a_complete = np.stack((a_infection, a_reinfection, a_reactivation), axis=3)

    return np.mean(a_complete, axis=1)


def format_risks(df_outcomes, df_pathogens, batch_values):
    """
    Format the output of the computation of partial and total indice risks.

    Parameters
    ----------
    df_outcomes: a data frame of the outcomes
    df_pathogens: a data frame with the characteristics of the pathogens
    batch_values: the assignation of calves to batches

    Reutrns
    -------
    A data frame with the partial and total indice risks

    Relationship with other functions
    ---------------------------------
    Calls the following function(s):
        -compute_risk

    """
    l_columns = [[prefix + '_' + suffix for
                  prefix in ['infection', 'reinfection', 'reactivation',
                             'total']] for
                 suffix in np.hstack((df_pathogens.index, 'total'))]

    df_output = pd.DataFrame(index=df_outcomes.index,
                             columns=np.hstack(l_columns))

    for batch in set(batch_values):
        # boolean whether the calf is in the batch
        is_batch = batch_values.index[batch_values == batch]
        # pathogen presence for the calves of the batch
        batch_status = a_status[:, :, df_outcomes.loc[is_batch,
                                                      "origin"]].swapaxes(0, 2)
        # risks for each calf
        a_risks = compute_risk(df_pathogens, batch_status)
        for i, idx in enumerate(is_batch):
            # compute the partial and total indices
            a_partial = np.vstack((a_risks[i], np.sum(a_risks[i], axis=0))).T
            a_total = np.vstack((a_partial, np.sum(a_partial, axis=0))).T
            # record the results
            df_output.at[idx] = a_total.flatten()

    return df_output


def check_results(df_expected, df_outcome):
    """
    Checks if the table of outcomes corresponds to the table of expected
    results. For the float results, the results are rounded to 1e-6, to prevent
    float errors.

    Parameters
    ----------
    df_expected: a table with the expected outcomes
    df_outcome: a table with the actual outcomes

    Returns
    -------
    Indicates if there are mismatches between the two tables, if not, prints
    'all outcomes correspond to expected'

    """
    print('Checking the test results:')
    no_error = True
    for col in df_expected.columns:
        l_errors = [i for i, row in df_outcome.iterrows() if
                    round(row.loc[col], 6) !=
                    round(df_expected.loc[i, col], 6)]
        for i in l_errors:
            break
            print("    - error in row '" + str(i) + "'and column '" +  col
                  + "'")
            no_error = False
    if no_error:
        print('    - all outcomes correspond to expected')


if __name__ == '__main__':

    # import modules
    import argparse
    import os
    import pandas as pd # version 0.23.4
    import numpy as np

    # argument parser for the script
    parser = argparse.ArgumentParser(description="""Script to generate the risk
                                     indices of the batches""")
    parser.add_argument('folder', type=str,
                        help="""the name of the folder containing the data""")
    parser.add_argument('-i', '--interval', type=int,
                        help="""the duration of the time period to consider at
                        once""")

    # get the parameters
    FOLDER = vars(parser.parse_args())['folder']
    is_test = FOLDER == 'test'

    if is_test:
        INTERVAL = 3
        data_path = './test/data/'
        outcome_path = './test/outcomes/expected_outcome.csv'
    else:
        data_path = './data/' + FOLDER
        try:
            INTERVAL = int(vars(parser.parse_args())['interval'])
        except:
            INTERVAL = 'no_interval'
        outcome_path = ('./outcomes/' + FOLDER + '/output_' + str(INTERVAL) +
                        '.csv')

    # load the dataset
    print("  Loading the data")

    # load the pathogen characteristics
    try:
        df_pathogens = pd.read_csv(data_path + '/pathogen_char.csv', index_col=0)
    except FileNotFoundError:
        raise ValueError('The pathogen values are missing')

    # load the outcomes of the assignation
    try:
        df_outcomes = pd.read_csv(outcome_path, index_col=0)
    except FileNotFoundError:
        raise ValueError('The outcomes are missing')

    is_old = 'batch_old' in df_outcomes.columns

    # load the status of the farms
    try:
        a_status = np.load(data_path + '/pathogen_status.npy')
    except FileNotFoundError:
        raise ValueError('The status of the farms are missing')


    print("  Computing the risks")
    # generate and format the risks for the optimized batches
    batch_values = df_outcomes.batch[~np.isnan(df_outcomes.batch)].astype('int')
    df_risks = format_risks(df_outcomes, df_pathogens, batch_values)

    # generate and format the risks for the old batches
    if is_old:
        print("      A distribution of old batches detected, computing risks")
        batch_values = df_outcomes.batch_old[~np.isnan(df_outcomes.batch_old)]
        df_risks_old = format_risks(df_outcomes, df_pathogens, batch_values)

    # save the output
    print("  Saving the output")
    os.makedirs(os.getcwd() + '/outcomes/' + FOLDER, exist_ok=True)
    df_risks.to_csv("./outcomes/" + FOLDER + "/risks_" + str(INTERVAL) + ".csv")
    if is_old:
        df_risks_old.to_csv("./outcomes/" + FOLDER + "/risks_" + str(INTERVAL) +
                            "_old.csv")

    # run a check of the results if the folder is 'test'
    if is_test:
        df_expected = pd.read_csv('./test/outcomes/expected_risks.csv',
                                  index_col=0)
        check_results(df_expected, df_risks)

##############################  END OF THE SCRIPT ##############################
