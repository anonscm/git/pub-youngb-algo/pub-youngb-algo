# This script produces standard graphical outputs for the batch optimization.
#
# Author: Thibaut Morel-Journel
#
# Written for R.6.0
#



hist_values <- function(values, res, limits) {
    #' Histogram generation
    #'
    #' @description Generate an array of values to plot histograms
    #'
    #' @usage hist_values(values, res, limits)
    #'
    #' @param values An array of values to use.
    #' @param res The resolution of the histogram (size of the intervals).
    #' @param limits The limit of the X-axis to consider
    #'
    #' @return A matrix with X values (the centre of each class of the histogram
    #' ) and Y values (the frequency of each class)

    # set the limits of the histogram values
    x_lims <- quantile(values, c(0, 1), na.rm=T)
    if(x_lims[1] > limits[1]){
        x_lims[1] <- limits[1]
    }
    if(x_lims[2] < limits[2]){
        x_lims[2] <- limits[2]
    }

    # generate the array of the distribution
    x_breaks <- (-1 : ((x_lims[2] + res - x_lims[1])/res) + .5) *res + x_lims[1]
    h_values <- hist(values, x_breaks, plot=F)
    array <- cbind(h_values$mids, h_values$counts/sum(h_values$counts))

    # return the output
    return(array[(array[, 1] >= limits[1]) & (array[, 1] <= limits[2]), ])
}

hist_plot <- function(l_values, res, xlab, colors, xlim=NA, bty="n", means=F) {
    #' Histogram generation
    #'
    #' @description Generate an array of values to plot histograms
    #'
    #' @usage hist_plot(l_values, res, xlab, colors, xlim=NA, bty="n", means=F)
    #'
    #' @param l_values A list of the values to consider
    #' @param res The resolution of the histogram (size of the intervals).
    #' @param xlab The label of the X-axis
    #' @param colors The color(s) to use for the plot of the different
    #' histograms
    #' @param xlim The limit of the X-axis to consider
    #' @param bty Should there be a box on the plot. Identical to the "bty"
    #' argument in par
    #' @param means Whether the mean values of each distribution should be
    #' displayed
    #'
    #' @return The function returns nothing but plots the distribution of each
    #' set of values in the list

    # generate the X-axis limits if non-existent
    if(is.na(xlim)){
        a_limits <- sapply(l_values, quantile, probs=c(.001, .999), na.rm=T)
        xlim <- c(min(a_limits[1, ]), max(a_limits[2, ]))
    }
    # compute the values
    l_hist <- lapply(l_values, hist_values, res=res, limits=xlim)
    # generate the limits of the y-axis
    ylim <- c(0, max(do.call(rbind, l_hist)[, 2]))
    # extrapolate the color values if unique
    if(length(colors) < length(l_values)){
        colors <- rep(colors, length(l_values))
    }
    # create the empty plot
    plot(NULL, xlim=xlim, ylim=ylim, xlab=xlab, ylab="Frequency", bty=bty)
    for(k in 1:length(l_hist)){
        # generate the polygon
        col_polygon <- paste0("#", paste(as.hexmode(col2rgb(colors[k])),
                                         collapse=""),
                              "30")
        x_polygon <- c(l_hist[[k]][, 1], rev(l_hist[[k]][, 1]))
        y_polygon <- c(l_hist[[k]][, 2], rep(0, nrow(l_hist[[k]])))
        polygon(x_polygon, y_polygon, col=col_polygon, border=NA)
    }
    # draw the points and lines
    for(k in 1:length(l_hist)){
        points(l_hist[[k]], pch=16, cex=.5, col=colors[k])
        lines(l_hist[[k]], col=colors[k])
    }
    if(means){
        segments(x0=sapply(l_values, mean, na.rm=T), y0=ylim[1], lty=2,
                 x1=sapply(l_values, mean, na.rm=T), y1=ylim[2], col=colors)
    }
}


cat("Generation of graphical files\n")

# check the arguments
#args = commandArgs(trailingOnly=TRUE)
args <- c("test")
if (length(args) < 1) {
    stop("The name of the folder must be provided")
}
FOLDER <- args[1]
is_test <- FOLDER == 'test'

if (length(args) < 2 & !is_test) {
    warning("  No interval was given in the arguments.")
    INTERVAL <- "no_interval"
} else {
    INTERVAL <- args[2]
}

# load the library quietly
suppressWarnings(suppressMessages(library(viridis)))

# load the data
cat("  - Loading the data\n")
if (is_test){
  path_data <- paste0('./test/data/dataset.csv')
  path_output <- paste0('./test/outcomes/expected_outcome.csv')
} else {
  path_data <- paste0('./data/', FOLDER, '/dataset.csv')
  path_output <- paste0('./outcomes/', FOLDER, '/output_', INTERVAL, '.csv')
}

df_data <- read.csv(path_data, row.names=1)
df_output <- read.csv(path_output, row.names=1)

# generate the table of batches
cat("  - Generating the list of batches\n")
df_batch <- as.data.frame(t(sapply(unique(df_output$batch), function(batch){
    selected <- df_output$batch == batch
    c(batch, unique(df_output[selected, 'batch_size']),
      unique(df_output[selected, 'nb_origins']),
      sd(df_data[selected, 'age'], na.rm=T),
      sd(df_data[selected, 'weight'], na.rm=T))
})))
colnames(df_batch) <- c('name', 'size', 'nb_origins', 'sd_age', 'sd_weight')
df_batch$sd_age[df_batch$sd_age == 0] <- NA
df_batch$sd_weight[df_batch$sd_weight == 0] <- NA
if(length(grep('_old', colnames(df_output)) == 3)){
    for(batch in unique(df_output$batch_old)){
        selected <- df_output$batch_old == batch
        nb_origins <-unique(df_output[selected, 'nb_origins_old'])
        sd_age <-  sd(df_data[selected, 'age'], na.rm=T)
        sd_weight <- sd(df_data[selected, 'weight'], na.rm=T)
        df_batch[df_batch$name == batch, 'nb_origins_old'] <- nb_origins
        df_batch[df_batch$name == batch, 'sd_age_old'] <- sd_age
        df_batch[df_batch$name == batch, 'sd_weight_old'] <- sd_weight
        df_batch$sd_age_old[df_batch$sd_age_old == 0] <- NA
        df_batch$sd_weight_old[df_batch$sd_weight_old == 0] <- NA
    }
}

#### Generate the figure of batch cahracteristics ####
cat("  - Generating the plots\n")

path_fig_charac <- paste0('./outcomes/', FOLDER, '/figure_charactersitics_',
                          INTERVAL, '.pdf')
pdf(path_fig_charac, width=20/2.54, height=7/2.54)
par(mfrow=c(1, 3), mar=c(5, 4, 0, 0), oma=c(0, 1, 2, 1), cex=.7)
for(k in 1:3){
    name <- c("nb_origins", "sd_age", "sd_weight")[k]
    res = c(1, 9, 9)[k]
    xlab <- c("Number of origins", "Standard deviation in age (days)",
              "Standard deviation in weight (kg)")[k]
    l_values <- lapply(grep(name, colnames(df_batch)), function(column){
        df_batch[, column]
    })
    hist_plot(l_values, res, xlab, colors=viridis(length(l_values), end = .85),
              means=T)
    mtext(LETTERS[k], side=3, adj=0)
}
graphics.off()
