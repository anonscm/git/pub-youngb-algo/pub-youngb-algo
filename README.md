# Batch optimization algorithm

Author: Thibaut Morel-Journel

Last update: 2020/11/25


## Get the algorithm

The git repository of the algorithm is available on SourceSup. It can be cloned
via SSH using the following command:

`git clone https://git.renater.fr/anonscm/git/pub-youngb-algo/pub-youngb-algo.git`


## General information

### Requirements

The algorithm is written with Python 3.7.2

The following modules are required:

* Argparse 1.1
* Itertools
* OS
* Pandas 0.24.2
* NumPy 1.16.2
* Sys

One of the additional scripts in written with R.6.0

The following library is required:

* Viridis

### Purpose

This repository contains an algorithm aimed at improving the creation of batches
of young bovines sent to fatteners. Based on a list of bovines sent during the
same period, their origin, their breed and the type of batch required by the
fattener, the algorithm assign young bovines to batches in order to minimize the
number of different origins represented in each batch.

### Structure of the repository

The folder in which is this README should include the following files:

```
.
|-- README.md
|-- scripts
	|-- compute_risks.py
	|-- generate_status.py
	|-- graphical_output.r
|-- src
	|-- __init__.py
	|-- __main__.py
	|-- algorithm.py
|-- test
	|-- data
    	|-- batch_list.csv
    	|-- dataset.csv
    	|-- pathogen_char.csv
    	|-- pathogen_status.npy
	|-- outcomes
		|-- expected_outcomes.csv
		|-- expected_risks.csv
```

## Run the algorithm

### Run the optimisation algorithm

To get the help, run the following from this folder:
`python3 -m src -h` or 

`python -m src -h` for Windows

The batch optimisation takes one positional argument: the name of the folder
containing the data used to perform the optimisation. **Warning**: The folder 
name *test* should only be used to perform the test of the algorithm. It can also 
take three optional arguments:
* -i: the duration of the time period to consider at once. If no time period is
given, the algorithm considers all the calves and the batches at once. 
* -c: if non-assigned batches and calves during a given time period should be 
carried over to the nex one.

The folder must contain the four following files:

* *dataset.csv*: a table with the id of animals in rows, indicating their breed, 
origin, date of arrival in the sorting centre, date of exit from the sorting 
centre.

* *batch_list.csv*: a table with the name of the batches to create in rows,
indicating the number of bovines required, the date of creation of the batch,
their destination, their main breed and the minimal proportion of animals of the
main breed 

To perform the optimisation, run the following from this folder: 

`python3 -m src -i<interval> -b -c <dataFolderName>` or 

`python -m src -i<interval> -b -c <dataFolderName>` for Windows

The output files created will be saved in a folder with the following path 
(automatically created if non-existent): `./outcomes/<dataFolderName>/output_<interval>.csv`.
This output file is a table with the id of the animals in rows, indicating their
origin, their batch, the size of the batch and the number of different origins of
the animals in the batch. If original batch compositions are provided in the
data, the old batches, sizes of batches and different number of different origins
will also be included in the output file.

### Test the algorithm

The test of the algorithm runs in approx. 0.55 second and the output files 
created weight approx. 4 KB. To test whether the algorithm functions properly, 
run the following from this folder: 

`python3 -m src test` or 

`python -m src test` for Windows

This command will run the algorithm and compare the outcomes with the expected
outcomes saved at the following path : `./test/expected_outcome.csv`. The
algorithm is properly functioning only if none of the outcomes differ. 


## Additional scripts

### Generate the status of the farms

This script can be used to generate a set of distributions of pathogens among
the farms considered according to their respective prevalences. 
The script takes two positional arguments: the name of the folder containing the
data and the number of repetitions of the generation required. It can also take
one optional argument:
* -c (--corr): to add if the presence of the pathogens in the different farms
should be positively correlated

To use the script, run the following from this folder: 

`python3 scripts/generate_status.py <dataFolderName> <numberOfRepetitions>` or 

`python scripts/generate_status.py <dataFolderName> <numberOfRepetitions>` for 
Windows

The output file created will be saved in the data folder with the following 
path: `./data/<dataFolderName>/pathogen_status.npy`. 

### Compute the risk indices

This script can be used on a dataset of outcomes, for which it computes the risk 
indices of the calves given a batch composition.
The script takes one positional argument: the name of the folder containing the 
data and the output used to perform the optimisation. It can also take one 
optional argument:
* -i: the duration of the time period to consider at once if it was also used in
the optimisation. If the value of this argument is not the same as the one used
for the algorithm, the output file will not be found.

To use the script, run the following from this folder: 

`python3 scripts/compute_risks.py -i<interval> -c <dataFolderName>` or 

`python scripts/compute_risks.py -i<interval> -c <dataFolderName>` for Windows

The output file created will be saved in a folder with the following path 
(automatically created if non-existent): `./outcomes/<dataFolderName>/risks_<interval>.csv`.
This output file is a table with the id of the animals in rows, indicating their
specific risks of naïve infection, reinfection and reactivation for each 
pathogen considered in *pathogen_char.csv*, the partial sums for each way over 
all pathogens, for each pathogen over all ways, and their total risk index. 
If an old batch composition is also included in the output file of the 
algorithm, an other output file will be created with the following path : 
`./outcomes/<dataFolderName>/risks_<interval>_old.csv`. This output file is the
same as the first one, but with information for the old batch composition.

### Generate graphical output

This script generates three graphics presenting the distribution of the number
of different origins, the standard deviation in age and the standard deviation
in weight of the batches. The script takes two arguments:
* the first one is the name of the folder containing the data and the output
* the second one is the duration of the time period to consider at once it was 
also used in the optimisation. This second argument is optional, the output file
will not be found if the value of this argument is not the same as the one used
for the algorithm.

To use the script, run the following from this folder: 

`Rscript scripts/graphical_output.r <dataFolderName> <interval>`

The output file created will be saved in the outcome's folder, with the 
following
path: `./outcomes/<dataFolderName>/graphical_output_<interval>.pdf`.

<!----------------------------- END OF THE README ----------------------------->
